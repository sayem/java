package com.sayem.module1.chapter13;

class A {
    // ...
}

class B extends A {
    // ...
}

class C extends A {
    // ...
}

// Note that D does NOT extend A.
class D {
    // ...
}