package com.sayem.module1.chapter10;

// Demonstrate System.out.write().
class WriteDemo {
    public static void main(String args[]) {
        int b;

        b = 'X';
        System.out.write(b);
        System.out.write('\n');
    }
}
