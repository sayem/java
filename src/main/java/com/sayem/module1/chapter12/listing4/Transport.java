package com.sayem.module1.chapter12.listing4;

// Demonstrate ordinal() and compareTo().

// An enumeration of Transport varieties.
enum Transport {
    CAR, TRUCK, AIRPLANE, TRAIN, BOAT
}
