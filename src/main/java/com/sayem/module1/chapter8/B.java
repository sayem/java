package com.sayem.module1.chapter8;

// B now includes meth1() and meth2() -- it adds meth3().
interface B extends A {
    void meth3();
}