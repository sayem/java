package com.sayem.module1.chapter7;

class OffRoad extends Vehicle {
    private int groundClearance; // ground clearance in inches

    OffRoad(int p, int f, int m) {
        super(p, f, m);
    }

    // ...
}