package com.sayem.module1.chapter3;

class DecrFor {
    public static void main(String args[]) {
        int x;

        for(x = 100; x > -100; x -= 5)
            System.out.println(x);
    }
}